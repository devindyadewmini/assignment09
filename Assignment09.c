#include <stdio.h>

int main() {

//Create a file named "assignment9.txt" and open it in write mode.
//Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.
    FILE *fptr;
    char write[1000]="UCSC is one of the leading institutes in Sri Lanka for computing studies.";
    fptr = fopen("assignment9.txt", "w");
    fprintf(fptr,write);
    fclose(fptr);

//Open the file in read mode and read the text from the file and print it to the output screen.
    fptr = fopen("assignment9.txt", "r");
    char string[100];
    while(fgets(string, 100, fptr))
        {
            printf("%s\n", string);
        }
    fclose(fptr);

//Open the file in append mode.
//Append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.
    fptr = fopen("assignment9.txt", "a");
    fputs("UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.",fptr);
    fclose(fptr);

    return 0;
}
